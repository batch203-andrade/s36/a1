// Require Installed modules
require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');

// Setup middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setup Connection with Mongoose
let db_url = process.env.DB_URL;
mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true });

// Connect to database
let db = mongoose.connection;
db.on("err", console.error.bind(console, "Unable to connect to database"));
db.once("open", () => console.log(`Successfully Authenticated to Database`));

// Setup Listen Port
const port = 3001;

// APP GOES BELOW

const userRoutes = require('./routes/userRoutes');
const taskRoutes = require('./routes/taskRoutes');
app.use('/users', userRoutes);
app.use('/tasks', taskRoutes);

// APP GOES ABOVE

// Start to listen 
app.listen(port, () => console.log(`Server is now running at port ${port}`));